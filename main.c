#include <stdio.h>
#include <stdlib.h>

int add(int a, int b)
{
    return a + b;
}

int minus(int a, int b)
{
    return a - b;
}

int time(int a, int b)
{
    return a * b;
}

int divide(int a, int b)
{
    return a / b;
}

#ifndef TEST
int main(int argc, char* argv[])
{
    printf("8 + 3 = %d\n", add(8, 3));
    return EXIT_SUCCESS;
}
#else
int test_add_5_3(void)
{
    int res = 0;
    res = add(5, 3);
    return res == 8;
}

int test_add_6_684(void)
{
    int res = 0;
    res = add(684, 6);
    return res == 690;
}

int main(int argc, char* argv[])
{
    if (!test_add_5_3())
    {
        printf("Erreur test_add()\n");
        return EXIT_FAILURE;
    }

    if (!test_add_6_684())
    {
        printf("Erreur test_add()\n");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif